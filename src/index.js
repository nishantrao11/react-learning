import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import Papa from "papaparse";
import { StyledEngineProvider } from '@mui/material/styles';
import { Game } from "./TicTacToe";
// import { EnhancedTable } from "./EnhancedTable";
import {BasicTable} from "./BasicTable"

// const SampleData = () => {
//   const [data, setData] = useState({});
//   Papa.parse("https://docs.google.com/spreadsheets/d/e/2PACX-1vSr1sZDC3HXPGFQk7427hUI20H6WISD9R4T9CBxYKbUuK30fpTDJHGgwnH8KsnBxNrZy_szjkxHGB6K/pub?output=csv", {
//     download: true,
//     header: true,
//     complete: (results) => {
//       setData(results.data);
//     },
//   });
//   const samples = Array.from(data);
//   return (
//     <ul>
//       {samples.map((data) => (
//         <li key={data.Number}>
//           {data.Person} , ({data.Org}) , {data.Dataset}, {data.StorageSystem}, {data.Cost}, {data.DatasetState}, {data.Usage}
//         </li>
//       ))}
//     </ul>
//   );
// };

function MyComponent() {
  const [showTable, setShowTable] = useState(false);
  return (
    <>
      <Game />
      <br></br>
      <React.StrictMode>
        <StyledEngineProvider injectFirst>
          <button onClick={() => setShowTable(!showTable)}>
            {showTable ? "Hide Table" : "Show Table"}
          </button>
          <br></br>
          {showTable && <BasicTable />}
        </StyledEngineProvider>
      </React.StrictMode>
    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<MyComponent />);