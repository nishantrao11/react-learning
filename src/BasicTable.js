import React, { useState } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Papa from "papaparse";

// function createData(name, calories, fat, carbs, protein) {
//   return { name, calories, fat, carbs, protein };
// }

// const rows = [
//   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//   createData('Eclair', 262, 16.0, 24, 6.0),
//   createData('Cupcake', 305, 3.7, 67, 4.3),
//   createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];

export function BasicTable() {
    const [data, setData] = useState({});
  Papa.parse("https://docs.google.com/spreadsheets/d/e/2PACX-1vSr1sZDC3HXPGFQk7427hUI20H6WISD9R4T9CBxYKbUuK30fpTDJHGgwnH8KsnBxNrZy_szjkxHGB6K/pub?output=csv", {
    download: true,
    header: true,
    complete: (results) => {
      setData(results.data);
    },
  });
  const samples = Array.from(data);


  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Person</TableCell>
            <TableCell align="right">Org</TableCell>
            <TableCell align="right">Dataset</TableCell>
            <TableCell align="right">Storage System</TableCell>
            <TableCell align="right">Cost</TableCell>
            <TableCell align="right">Dataset State</TableCell>
            <TableCell align="right">Usage</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {samples.map((row) => (
            <TableRow
              key={row.Number}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.Person}
              </TableCell>
              <TableCell align="right">{row.Org}</TableCell>
              <TableCell align="right">{row.Dataset}</TableCell>
              <TableCell align="right">{row.StorageSystem}</TableCell>
              <TableCell align="right">{row.Cost}</TableCell>
              <TableCell align="right">{row.DatasetState}</TableCell>
              <TableCell align="right">{row.Usage}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
// {data.} , ({data.}) , {data.}, {data.}, {data.}, {data.}, {data.}
